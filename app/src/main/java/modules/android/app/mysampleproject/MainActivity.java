package modules.android.app.mysampleproject;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {

    android.support.v7.widget.Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
    }

}
